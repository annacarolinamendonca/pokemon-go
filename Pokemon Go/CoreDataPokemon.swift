//
//  CoreDataPokemon.swift
//  Pokemon Go
//
//  Created by Anna Carolina Ribeiro Mendonça on 10/08/19.
//  Copyright © 2019 Anna Carolina Ribeiro Mendonça. All rights reserved.
//

import UIKit
import CoreData

class CoreDataPokemon {
    
    //recuperar o context
    func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let context = appDelegate?.persistentContainer.viewContext
        return context!
        
    }
    //capturar pokemon
    
    func salvarPokemon(pokemon: Pokemon){
        let context = self.getContext()
        pokemon.capturado = true
        
        do{
            try context.save()
        }catch{}
    }
    
    //adicionar todos os pokemon
    func adicionarTodosPokemon() {
        
        let context = self.getContext()
        
        self.criarPokemon(nome: "Pikachu", nomeImagem: "pikachu-2", capturado: true )
        self.criarPokemon(nome: "Squirtle", nomeImagem: "squirtle", capturado: false  )
        self.criarPokemon(nome: "Charmander", nomeImagem: "charmander", capturado: false  )
        self.criarPokemon(nome: "Caterpie", nomeImagem: "caterpie", capturado: false  )
        self.criarPokemon(nome: "Bullbasaur", nomeImagem: "bullbasaur", capturado: false  )
        self.criarPokemon(nome: "Bellsprout", nomeImagem: "bellsprout", capturado: false  )
        self.criarPokemon(nome: "Psyduck", nomeImagem: "psyduck", capturado: false  )
        self.criarPokemon(nome: "Rattata", nomeImagem: "rattata", capturado: false  )
        self.criarPokemon(nome: "Meowth", nomeImagem: "meowth", capturado: false  )
        self.criarPokemon(nome: "Snorlax", nomeImagem: "snorlax", capturado: false  )
        self.criarPokemon(nome: "Zubat", nomeImagem: "zubat", capturado: false  )
        
        do{
            try context.save()
        }catch{}
        
    }
    
    //criar os pokemon
    func criarPokemon(nome:String, nomeImagem: String, capturado: Bool ) {
        let context = self.getContext()
        let pokemon = Pokemon(context: context)
        pokemon.nome = nome
        pokemon.nomeImagem = nomeImagem
        pokemon.capturado = capturado
        
    }
    
    func recuperarTodosPokemon() -> [Pokemon] {
        
        let context = self.getContext()
        
        do {
            let pokemon = try context.fetch(Pokemon.fetchRequest() ) as! [Pokemon]
            
            if pokemon.count == 0 {
                self.adicionarTodosPokemon()
                return recuperarTodosPokemon()
            }
            
            return pokemon
        } catch  {}
        
        return []

    }
    
    func recuperarPokemonCapturado(capturado: Bool) -> [Pokemon] {
        let context = self.getContext()
    
        let requisicao = Pokemon.fetchRequest() as NSFetchRequest<Pokemon>
        requisicao.predicate = NSPredicate(format: "capturado = %@", NSNumber(value: capturado))
        
        do {
            let pokemons = try context.fetch(requisicao) as [Pokemon]
            return pokemons
            
        } catch  {}
        
        return []
        
    }
}
