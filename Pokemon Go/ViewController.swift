//
//  ViewController.swift
//  Pokemon Go
//
//  Created by Anna Carolina Ribeiro Mendonça on 08/08/19.
//  Copyright © 2019 Anna Carolina Ribeiro Mendonça. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController,MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapa: MKMapView!
    var gerenciadorLocalizacao = CLLocationManager ()
    var contador = 0
    var coreDataPokemon: CoreDataPokemon!
    var pokemon: [Pokemon] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapa.delegate = self
        gerenciadorLocalizacao.delegate = self
        //solicitacao de autorizacao
        gerenciadorLocalizacao.requestWhenInUseAuthorization()
        gerenciadorLocalizacao.startUpdatingLocation()
        
        // recuperar os pokemon
        self.coreDataPokemon = CoreDataPokemon()
        self.pokemon = self.coreDataPokemon.recuperarTodosPokemon()
        
        // exibir anotaoes(pokemon) aleatorias no mapa
        Timer.scheduledTimer(withTimeInterval: 10, repeats: true) { (timer) in
            
            if let coordenadas = self.gerenciadorLocalizacao.location?.coordinate {
                
                let totalPokemon = UInt32(self.pokemon.count)
                let indicePokemonAleatorio = arc4random_uniform(totalPokemon)
                
                let pokemon = self.pokemon[ Int (indicePokemonAleatorio) ]
                
                let anotacao = PokemonAnotacao(coordenadas: coordenadas,  pokemon: pokemon )
                
                let latitudeAleatoria = (Double(arc4random_uniform(400)) - 200) / 100000.0
                let longitudeAleatoria = (Double(arc4random_uniform(400)) - 200) / 100000.0
                
                anotacao.coordinate.latitude += latitudeAleatoria
                anotacao.coordinate.longitude += longitudeAleatoria
                
                self.mapa.addAnnotation(anotacao)
            }
            
        }
        
    }
    // exibindo as imagens dos pokemon
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let anotacaoView = MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
        
        //definir a imagem do player
        if annotation is MKUserLocation {
            anotacaoView.image = UIImage (named: "player")
            
        }else{
            
            let pokemon = (annotation as! PokemonAnotacao).pokemon
            anotacaoView.image = UIImage (named: pokemon.nomeImagem!)
            
        }
        
        
        // definir um tamanho para a anotacao
        var frame = anotacaoView.frame
        frame.size.height = 40
        frame.size.width = 40
        
        anotacaoView.frame = frame
        
        return anotacaoView
    }
    
    // selecionar uma anotacao
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        let anotacao = view.annotation
        let pokemon = (view.annotation as! PokemonAnotacao).pokemon
        
        mapView.deselectAnnotation(anotacao, animated: true)
        
        if anotacao is MKUserLocation {
            return
        }
        
        if let coordAnotacao = anotacao?.coordinate{
            let regiao = MKCoordinateRegion.init(center: coordAnotacao, latitudinalMeters: 500, longitudinalMeters: 500)
            mapa.setRegion(regiao, animated: true)
        }
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (timer) in
            
            if let coord = self.gerenciadorLocalizacao.location?.coordinate {
                
                if self.mapa.visibleMapRect.contains( MKMapPoint(coord)) {
                    self.coreDataPokemon.salvarPokemon(pokemon: pokemon)
                    // remover a anotacao quando capturar pokemon
                    self.mapa.removeAnnotation(anotacao!)
                    
                    // exibir a mensagem quando capturar o pokemon
                    let alertController = UIAlertController(title: "Parabéns !!",
                                                            message: "Voce capturou o: \(pokemon.nome!)", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alertController.addAction(ok)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }else{
                    let alertController = UIAlertController(title: "Voce esta longe demais ",
                                                            message: "Voce precisa se aproximar mais para capturar o: \(pokemon.nome!)", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alertController.addAction(ok)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
        

    }
    
    
    //centralizando a localizacao no mapa e para parar de centralizar automaticamento o usuario
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        if contador < 3 {
            self.centralizar()
            contador += 1
            
        }else{
            gerenciadorLocalizacao.stopUpdatingLocation()
        }
    }
    
    // para capturar quando o usuário autoriza ou não o acesso a localização.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status != .authorizedWhenInUse && status != .notDetermined{
            
            // alerta
            
            let alertaController = UIAlertController(title: "Permissnao de autorização", message: "É necessário permissão para acesso a sua localização para voce se tornar um mestre Pokemon", preferredStyle: .alert)
            
            // botoes de ação
            
            let acaoConfiguracoes = UIAlertAction (title: "Abrir Configurações", style: .default) { (alertaConfiguracoes) in
                
                if let configuracoes = NSURL(string: UIApplication.openSettingsURLString){
                    UIApplication.shared.open(configuracoes as URL)
                }
            }
            
            let acaoCancelar = UIAlertAction (title: "Cancelar", style: .default, handler: nil)
            
            alertaController.addAction(acaoConfiguracoes)
            alertaController.addAction(acaoCancelar)
            
            present(alertaController, animated: true, completion: nil)
            
        }
    }
    //centralizando a localizacao no mapa e para parar de centralizar automaticamento o usuario
    func centralizar(){
        if let coordenadas = gerenciadorLocalizacao.location?.coordinate{
            let regiao = MKCoordinateRegion.init(center: coordenadas, latitudinalMeters: 500, longitudinalMeters: 500)
            mapa.setRegion(regiao, animated: true)
        }
    }

    @IBAction func centralizarJogador(_ sender: Any) {
        self.centralizar() // centralizar a localizacao do usuario
        
    }
    
    @IBAction func abrirPokedex(_ sender: Any) {
    }
}

