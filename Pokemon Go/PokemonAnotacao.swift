//
//  PokemonAnotacao.swift
//  Pokemon Go
//
//  Created by Anna Carolina Ribeiro Mendonça on 10/08/19.
//  Copyright © 2019 Anna Carolina Ribeiro Mendonça. All rights reserved.
//

import UIKit
import MapKit

class PokemonAnotacao : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var pokemon: Pokemon
    
    init(coordenadas: CLLocationCoordinate2D, pokemon: Pokemon) {
        self.coordinate = coordenadas
        self.pokemon = pokemon
    }
    
}
