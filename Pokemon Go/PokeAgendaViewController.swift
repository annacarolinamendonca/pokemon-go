//
//  PokeAgendaViewController.swift
//  Pokemon Go
//
//  Created by Anna Carolina Ribeiro Mendonça on 10/08/19.
//  Copyright © 2019 Anna Carolina Ribeiro Mendonça. All rights reserved.
//

import UIKit

class PokeAgendaViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var pokemonCapturados: [Pokemon] = []
    var pokemonNaoCapturados: [Pokemon] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //recuperar os pokemon capturados e nao capturados
        let coreDataPokemon = CoreDataPokemon()
        
        self.pokemonCapturados = coreDataPokemon.recuperarPokemonCapturado(capturado: true)
        self.pokemonNaoCapturados = coreDataPokemon.recuperarPokemonCapturado(capturado: false)
    }
    // exibindo pokemon capturados e nao capturados
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section  == 0 {
            return "Capturados"
        }else{
            return "Não Capturados"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return self.pokemonCapturados.count
        }else{
           return self.pokemonNaoCapturados.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let pokemon: Pokemon
        
        if indexPath.section == 0{
            pokemon = self.pokemonCapturados[indexPath.row]
        }else{
            pokemon = self.pokemonNaoCapturados[indexPath.row]
            
        }
        
        let celula  = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "celula")
        celula.textLabel?.text = pokemon.nome // exibir o nome
        celula.imageView?.image = UIImage (named: pokemon.nomeImagem!) // exibir a imagem
        
        return celula
    }
    
    @IBAction func voltarMapa(_ sender: Any) {
        //voltar para o mapa
        dismiss(animated: true, completion: nil)
    }
    

}
